package big.rabbitmq.api.dlx;

import java.util.HashMap;
import java.util.Map;

import big.rabbitmq.constant.ConnectionConstant;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Consumer {

	
	public static void main(String[] args) throws Exception {

        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(ConnectionConstant.HOST);
        connectionFactory.setPort(ConnectionConstant.PORT);
        connectionFactory.setVirtualHost(ConnectionConstant.VIRTUALHOST);
		
		Connection connection = connectionFactory.newConnection();
		Channel channel = connection.createChannel();
		
		// 这就是一个普通的交换机 和 队列 以及路由
		String exchangeName = "test_dlx_exchange";
		String routingKey = "dlx.#";
		String queueName = "test_dlx_queue";
		
		channel.exchangeDeclare(exchangeName, "topic", true, false, null);

        // 死信队列
        String dlxExchangeName = "dlx.exchange";
        String dlxQueueName = "dlx.queue";


		Map<String, Object> agruments = new HashMap<String, Object>();
		agruments.put("x-dead-letter-exchange", dlxExchangeName);
		//这个agruments属性，要设置到声明队列上
		channel.queueDeclare(queueName, true, false, false, agruments);
		channel.queueBind(queueName, exchangeName, routingKey);
		
		//要进行死信队列的声明:
		channel.exchangeDeclare(dlxExchangeName, "topic", true, false, null);
		channel.queueDeclare(dlxQueueName, true, false, false, null);
		channel.queueBind(dlxQueueName, dlxExchangeName, "#");
		
		channel.basicConsume(queueName, true, new MyConsumer(channel));
		
		
	}
}
