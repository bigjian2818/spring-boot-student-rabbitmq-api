package big.rabbitmq.api.consumer;

import big.rabbitmq.constant.ConnectionConstant;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Producer {

	
	public static void main(String[] args) throws Exception {

        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(ConnectionConstant.HOST);
        connectionFactory.setPort(ConnectionConstant.PORT);
        connectionFactory.setVirtualHost(ConnectionConstant.VIRTUALHOST);
		
		Connection connection = connectionFactory.newConnection();
		Channel channel = connection.createChannel();
		
		String exchange = "test_consumer_exchange";
		String routingKey = "consumer.save";
		
		String msg = "Hello RabbitMQ Consumer Message";
		
		for(int i =0; i<5; i ++){
		    Thread.sleep(2000);
			channel.basicPublish(exchange, routingKey, true, null, msg.getBytes());
		}
		
	}
}
