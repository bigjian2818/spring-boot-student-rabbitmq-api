package big.rabbitmq.api.exchange.direct;

import big.rabbitmq.constant.ConnectionConstant;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Producer4DirectExchange {

	
	public static void main(String[] args) throws Exception {
		
		//1 创建ConnectionFactory
		ConnectionFactory connectionFactory = new ConnectionFactory();
		connectionFactory.setHost(ConnectionConstant.HOST);
		connectionFactory.setPort(ConnectionConstant.PORT);
		connectionFactory.setVirtualHost(ConnectionConstant.VIRTUALHOST);
		
		//2 创建Connection
		Connection connection = connectionFactory.newConnection();
		//3 创建Channel
		Channel channel = connection.createChannel();  
		//4 声明
		String exchangeName = "test_direct_exchange";
		String routingKey = "test.direct";
        String routingKey2 = "test.direct2";
		//5 发送
		for (int i = 0; i > -1; i++) {
		    Thread.sleep(2000);
            String msg = "Hello World RabbitMQ 4  Direct Exchange Message ... " + getFormat(new Date());
            channel.basicPublish(exchangeName, routingKey , null , msg.getBytes());
            msg = "Hello World RabbitMQ 4  Direct Exchange Message 2 ... " + getFormat(new Date());
            channel.basicPublish(exchangeName, routingKey2 , null , msg.getBytes());
        }
	}

	public  static  String getFormat(Date date) {
        DateFormat bf = new SimpleDateFormat("yyyy-MM-dd E a HH:mm:ss");//多态
        //2017-04-19 星期三 下午 20:17:38
        String format = bf.format(date);//格式化 bf.format(date);
        return format;
    }
	
}
