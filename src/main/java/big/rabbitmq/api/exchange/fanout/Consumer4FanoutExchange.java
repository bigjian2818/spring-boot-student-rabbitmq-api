package big.rabbitmq.api.exchange.fanout;

import big.rabbitmq.constant.ConnectionConstant;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.QueueingConsumer.Delivery;

public class Consumer4FanoutExchange {

	public static void main(String[] args) throws Exception {

        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(ConnectionConstant.HOST);
        connectionFactory.setPort(ConnectionConstant.PORT);
        connectionFactory.setVirtualHost(ConnectionConstant.VIRTUALHOST);

        connectionFactory.setAutomaticRecoveryEnabled(true);
        connectionFactory.setNetworkRecoveryInterval(3000);
        Connection connection = connectionFactory.newConnection();
        
        Channel channel = connection.createChannel();  
		//4 声明
		String exchangeName = "test_fanout_exchange";
		String exchangeType = "fanout";
        String queueName = "test_fanout_queue";
        String queueName2 = "test_fanout_queue";
		String routingKey = "";	//不设置路由键
		channel.exchangeDeclare(exchangeName, exchangeType, true, false, false, null);
        channel.queueDeclare(queueName, false, false, false, null);
        channel.queueDeclare(queueName2, false, false, false, null);
        channel.queueBind(queueName, exchangeName, "");
        channel.queueBind(queueName2, exchangeName, "123");

        //durable 是否持久化消息
        QueueingConsumer consumer = new QueueingConsumer(channel);
        QueueingConsumer consumer2 = new QueueingConsumer(channel);
        //参数：队列名称、是否自动ACK、Consumer
        channel.basicConsume(queueName, true, consumer);
        channel.basicConsume(queueName, true, consumer2);
        //循环获取消息
        while(true){  
            //获取消息，如果没有消息，这一步将会一直阻塞  
            Delivery delivery = consumer.nextDelivery();  
            String msg = new String(delivery.getBody());    
            System.out.println("收到消息：" + msg);

            Delivery delivery2 = consumer2.nextDelivery();
            String msg2 = new String(delivery2.getBody());
            System.out.println("收到消息：" + msg2);
        } 
	}
}
