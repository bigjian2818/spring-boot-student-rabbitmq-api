package big.rabbitmq.quickstart;

import big.rabbitmq.constant.ConnectionConstant;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * 生产端
 */
public class Procuder {

	
	public static void main(String[] args) throws Exception {
		//1 创建一个ConnectionFactory, 并进行配置
		ConnectionFactory connectionFactory = new ConnectionFactory();
		connectionFactory.setHost(ConnectionConstant.HOST);
		connectionFactory.setPort(ConnectionConstant.PORT);
		connectionFactory.setVirtualHost(ConnectionConstant.VIRTUALHOST);
		
		//2 通过连接工厂创建连接
		Connection connection = connectionFactory.newConnection();
		
		//3 通过connection创建一个Channel
		Channel channel = connection.createChannel();
		
		//4 通过Channel发送数据
		for(int i=0; i < 5000; i++){
			String msg = "Hello RabbitMQ!" + i;
			//1 exchange   2 routingKey
			channel.basicPublish("", "test001", null, msg.getBytes());
            //System.err.println("生产端: " + msg);
            Thread.sleep(200);
		}

		//5 记得要关闭相关的连接
		channel.close();
		connection.close();
	}
}
